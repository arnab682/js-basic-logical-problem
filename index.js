let n = Boolean(100);
//Number(n);
console.log(typeof n);

let num = String(1000);
console.log(typeof n);
console.log(num);

let b = String(true);
console.log(typeof b);
console.log(b);

let f = String(function(){}); //empty function
console.log(typeof f);

let o = String({}); //empty object
console.log(typeof o);
console.log(o);

console.log('Hello' + 12);
console.log(2 + 5 + 'Hello' + 12);

String(123) // '123'
String(-123) // '-123'
String(null) // 'null'
String(undefined) // 'undefined'
String(true)  // 'true'
String(false)  // 'false'

// Boolean(2) //explicit
// if(2){} // implicit
// !!2
// 2 || 'Hello'

// return false =>
Boolean('');
Boolean(0);
Boolean(-0);
Boolean(NaN);
Boolean(undefined);
Boolean(false); //all return false


//return true 
Boolean([]); //empty array
Boolean({}); //empty object
Boolean(function(){}) //empty function



//Numaric Conversion
// >, <, >=, =<
// | - ^ &
// + - / *


// Binary plus operator
console.log('abr'+100);

//unary plus operator
console.log( +'abc'); // unary plus every string convert to number

let t = +'1234'; //string to number convert
console.log(typeof t); //number

console.log(100 == '100'); // two number
console.log('123' == '123'); // two string


console.log(Number('100'));
console.log(typeof '321');
console.log(typeof +'321');
console.log(4>'5');
console.log(5/null); // null convert to 0
console.log(true | 0);


console.log(Number(null));
console.log(Number(undefined));
console.log(Number(true));
console.log(Number(false));
console.log(Number(' 4244  '));
console.log(Number('-12.333'));
console.log(Number('\n'));
console.log(Number('33edc'));
console.log(Number(1243));


console.log(null == null);
console.log(null == undefined);
console.log(null == false);
console.log(null == true);
console.log(null == 450);
console.log(null == 'str');
console.log(null == 'null');


console.log(NaN == NaN);
console.log(NaN == 'NaN');
console.log(NaN == false);
console.log(NaN == true);



//unexpected result
console.log([1] + [2,3]);

console.log(Boolean({}));
console.log(Boolean([]));

let obj = {};
obj.toString(); //String Convertion

//Ex: 1
console.log(true + false); // 1+0 =1
console.log(12/'6'); // 12/6 = 2

//Ex: 3
console.log('number' + 15 + 3); // string + number + number
// 'number' + '15' + 3  // string + string + number
// 'number15' + 3  // string + number 
// 'number15' + '3' // string + string
// 'number153'  // string

//Ex: 4
console.log(15 + 3 + 'number' ); //number + number + string
// 18 + 'number' // binary + or bainary arithmatic oparator
// '18' + 'number'
// '18number'


//Ex: 5
// [1] > null // numaric conversion
console.log([1]);
console.log(String[1]);
console.log([1, 2, 3]);
// [1] > null
// '1' > null //object,array automatic make a tostring
// 1 > 0 //numaric convertion


//Ex: 6
//'foo' + + 'bar'
//'foo' + (+'bar') // binary + (Unary +)
//'foo' + NaN
//'foo' + 'NaN' //bainary +
//'fooNaN'
console.log('foo' + + 'bar');


//Ex: 7
// 'true' == true //numaric number convert
// NaN == 1
// false
console.log('true' == true);
console.log('false' == false);


//Ex: 8
// null == ''
// false
console.log(null == '');

//Ex: 9
// !!'false' == !!'true'
// !!true == !!true
// !false == !false
// true == true
// 1 == 1
// true
console.log(!!'false' == !!'true');

//Ex: 10
//['x'] == 'x'
// 'x' == 'x'
// true
console.log(['x'] == 'x');


//Ex: 11
// [] + null + 1
// '' + null + 1
// 'null' + 1
// 'null' + '1'
// 'null1'
console.log([] + null + 1);

//Ex: 12
// 0 || '0' && {}
// (0 || '0') && {}
// (false || true) && {} //internally
// '0' && {}
// true && true //Internally
// {}
console.log(0 || '0' && {});

// Ex: 13
// {} +[] + {} + [1]
//    +[] + {} + [1]
// 0 + {} + [1]
// 0 + '[object object]' + [1]
// '0[object object]1'
console.log({} +[] + {} + [1]);

// Ex: 14
// ! +[] + [] + ![]
//   (!+[]) + [] + (![])
//       (!0) + [] + false
// true + [] + false
// true + '' + false
// 'true' + '' + 'false'
// 'truefalse'
console.log(! +[] + [] + ![]);

//Ex: 15
// [] + [] + 'foo'.split('')
//     [] + [] + ['f', 'o', 'o']
// '' + '' + 'f,o,o'
// 'f,o,o'
console.log([] + [] + 'foo'.split(''));

// Ex: 16
// ('b' + 'a' + + 'a' + 'a').toLocaleLowerCase()
// ('b' + 'a' + (+'a') + 'a').toLocaleLowerCase()
// ('b' + 'a' + NaN + 'a').toLocaleLowerCase()
// ('baNaNa').toLocaleLowerCase();
// 'banana'
console.log(('b' + 'a' + + 'a' + 'a').toLocaleLowerCase());