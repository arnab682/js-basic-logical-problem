var name = 'Arnab Das';
var age = 25;
var skills = ['Java', 'Javascript', 'Angular', 'React'];

console.log(name);
console.log(age);
console.log(skills);

console.log(undefined == null);
console.log(undefined === null);
console.log(100 == '100');
console.log(100 === '100');

//Deffarent type of Data type
//Number , String, Boolean, 
//undefined, null
//Array, Object, Function

var num = 1;
console.log(num);

var double = 7.25;
console.log(double);

var str = 'Arnab';
console.log(str);
console.log(str.toUpperCase());
console.log(str.toLowerCase());

var data = [1, 2, 3, 4];
console.log(data.length);

var arr = ['Arnab', 'Das', 'Anik'];
console.log(arr);

for(i = 0; i < arr.length; i++){
  console.log(arr[i]);
}

while(i < arr.length){
  console.log(arr[i]);
  i++;
  }
  
//  do{

//   i = 0;
//   console.log(arr[i]);
//   i++;
//  }while(i < arr.length);

var age = 18;
if(18 <= age) {
  console.log("You are adult " + age);
} else {
    console.log("You are not adult");
}

console.log("My Crush is Antika");

var name = [];
name[0] = 'Arnab';
name[1] = 'Das';
name[2] = 'Anik';
console.log("Hello " + name[0] + " " + name[1] + " " + name[2]);


var names = ['Arnab', 'Anik', 'Anika', 'Antika', 'Ami', 'Aditi'];
var data = [1, 2, 3, 4, 5, 6];

console.log(names.length);
console.log(names[4]);
console.log(names[names.length - 1]);

console.log(names.indexOf('Antika'));
names[names.length] = 'Umma';
console.log(names);
names.push('love');
console.log(names);

var sortArray = names.sort();
console.log(sortArray);

names.splice(6,3);
console.log(names);

names.splice(6,3, 'ooo');
console.log(names);



/// Function
// function diglaration
function add(a, b){
  return a + b;
}
console.log(add(2,4));


// function expation
var add = function(a, b){
  return a + b;
}
console.log(add(10, 12));

var add = function(a, b){
  return a + b;
}
var addition = add;
console.log(addition(10, 12));


//callback function
var names1 = ['Arnab', 'Anik', 'Anika', 'Antika', 'Ami', 'Aditi'];

names.forEach(function(name1){
  console.log(name1);
});

names.forEach(print);
function print(name1){
  console.log(name1);
  // var n = names.toUpperCase();
  // console.log(n);
}


function sum(x, y){
  var p = x +y;
  return p;
}
console.log(sum(10,2));


function sub(y, x){
  var q = y-x;
  submition(q);
}
function submition(o){
  console.log(o);
}
sub(20,10);


// ........................Callback..................... //
function operation(x, y, callback){
  var p = x+y;
  var q = x-y;
  callback(p,q);
}
function print(c, d){
  console.log(c,d);
}
function multiply(e, f){
  console.log(e*f);
}

operation(10, 5, print);
operation(10, 5, multiply);

//....or....//

function opera(x, y, callback){
  var p = x+y;
  var q = x-y;
  callback(p,q);
}
opera(10, 5, function(u, v){
  console.log('Divition: ' + (u/v));
});
opera(10, 5, function(u, v){
  console.log('Maltiply: ' + (u*v));
});

// ........................Callback..................... //

// extra
function add(x,y, callback){
  callback(x,y);
}
function sum(a,b){
  console.log(a+b);
}
add(10,2, sum);


//2
//4
function number(q,w, callback){
  callback(q,w);
}
function devision(e,r){
  console.log(e/r);
}
function multiply(u,i){
  console.log(u*i);
}
number(12,6, devision);
number(4,1, multiply);

// or
function add(x,y, callback){
  callback(x,y);
}
function sum(a,b){
  console.log(a+b);
}
add(10,2, function(a,b){
  console.log(a+b);
});


//2
//4
function number(q,w, callback){
  callback(q,w);
}

number(12,6, function(e,r){
  console.log(e/r);
});
number(4,1, function(u,i){
  console.log(u*i);
});

//.......................